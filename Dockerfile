FROM composer:1.6.5 as build

WORKDIR /app
COPY . /app
RUN composer install

FROM php:7.1.8-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql

EXPOSE 80:8888
COPY --from=build /app /app
COPY vhost.conf /etc/apache2/sites-available/000-default.conf
COPY .env.deploy .env
RUN chown -R www-data:www-data /app \
    && a2enmod rewrite

# copy run.sh /tmp
# RUN ["chmod", "+x", "/tmp/run.sh"]
# ENTRYPOINT ["/tmp/run.sh"]
