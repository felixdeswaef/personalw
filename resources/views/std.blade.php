<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title','felix de swaef')</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="js/bootstrap.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Bangers|Permanent+Marker|Rock+Salt" rel="stylesheet">
    <link rel="stylesheet" href="css/head.css">

    <script src="js/head.js"></script>

</head>
<header>

    <div class="headp shrinker">
        <h1 class="headt shrinker">FELIX DE SWAEF</h1>
        <nav class="nav-wrap shrinker">
            <ul class="group " id="example-one">

                <li class="current_page_item"><a href="#">About</a></li>
                <li><a href="#">Photography</a></li>
                <li><a href="#">Software</a></li>
                <li><a href="#">Hardware</a></li>
                <li><a href="#">Visuals</a></li>
                <li><a href="#">Portal</a></li>
            </ul>
        </nav>
    </div>



</header>
<body>
    @yield('content')
</body>
</html>