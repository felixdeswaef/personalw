<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class foodbillitems extends Model
{
    protected $fillable = [
        'subselect','count', 'billid', 'itemid','username','userid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    public  function food()
    {
        return $this->hasOne(fooditems::class);
    }
    public  function bill()
    {
        return $this->hasOne(foodbills::class);
    }

}
