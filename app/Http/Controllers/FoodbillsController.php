<?php

namespace App\Http\Controllers;

use App\foodbills;
use Illuminate\Http\Request;

class FoodbillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $place = foodbills::all();

        return response()->json($place);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $place = foodbills::create($request->json()->all());

        return response()->json([
            'message' => 'Great success! New task created',
            'task' => $place
        ]);
    }

    public function show(foodbills $place)
    {
        return $place;
    }

    public function update(Request $request, foodbills $place)
    {
        $request->validate([
            'title'       => 'nullable',
            'description' => 'nullable'
        ]);

        $place->update($request->all());

        return response()->json([
            'message' => 'Great success! Task updated',
            'task' => $place
        ]);
    }

    public function destroy(foodbills $place)
    {
        $place->delete();

        return response()->json([
            'message' => 'Successfully deleted task!'
        ]);
    }
}