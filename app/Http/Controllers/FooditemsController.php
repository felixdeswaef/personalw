<?php

namespace App\Http\Controllers;

use App\fooditems;
use Illuminate\Http\Request;

class FooditemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $place = fooditems::all();

        return response()->json($place);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $place = fooditems::create($request->json->all());

        return response()->json([
            'message' => 'Great success! New item created',
            'task' => $place
        ]);
    }

    public function show(fooditems $place)
    {
        return $place;
    }

    public function update(Request $request, fooditems $place)
    {
        $request->validate([
            'title'       => 'nullable',
            'description' => 'nullable'
        ]);

        $place->update($request->all());

        return response()->json([
            'message' => 'Great success! Task updated',
            'task' => $place
        ]);
    }

    public function destroy(fooditems $place)
    {
        $place->delete();

        return response()->json([
            'message' => 'Successfully deleted task!'
        ]);
    }
}