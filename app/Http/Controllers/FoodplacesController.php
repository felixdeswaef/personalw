<?php

namespace App\Http\Controllers;

use App\foodplaces;
use Illuminate\Http\Request;

class FoodplacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $place = foodplaces::all();

        return response()->json($place);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



        $place = foodplaces::create($request->json()->all());

        return response()->json([
            'message' => 'Great success! New task created',
            'place' => $place
        ]);
    }

    public function show(foodplaces $place)
    {
        return  $place;
    }

    public function update(Request $request, foodplaces $place)
    {
        $request->validate([
            'title'       => 'nullable',
            'description' => 'nullable'
        ]);

        $place->update($request->all());

        return response()->json([
            'message' => 'Great success! Task updated',
            'task' => $place
        ]);
    }

    public function destroy(foodplaces $place)
    {
        $place->  delete();

        return response()->json([
            'message' => 'Successfully deleted task!'
        ]);
    }
}