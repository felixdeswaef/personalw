<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\str;
use App\User;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

class APILogin extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */



    public function login(Request $request)
    {

        if($request->json()->get('user')==null){
            return response()->json([
                'message' => 'invalid',
                'spe'=>'uname',

            ]);
        }
        try {
            $u = User::where('name', $request->json()->get('user'))->first();
        } catch (Exception $e) {

            return response()->json([
                'message' => $e,

            ]);
        }
        if ($u==null){
            return response()->json([
                'message' => 'NONEXISTANT',

            ]);
        }

        if (Hash::check($request->json()->get('pwd'), $u->password)) {
            return response()->json([
                'message' => 'ACCEPTED',
                'apikey' => $u->api_token,
                'NAME' => $u->name
            ]);
        }
        return response()->json([
            'message' => 'Refused',

        ]);


    }
}
