<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class foodbills extends Model
{
    //
    protected $fillable = [
        'name', 'placeid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    public function has()
    {
        return $this->hasMany(foodbillitems::class);
    }

}

