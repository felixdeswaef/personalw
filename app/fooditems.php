<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fooditems extends Model
{
    //
    protected $fillable = [
        'name', 'price', 'placeid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    public function place()
    {
        return $this->belongsTo(foodplaces::class);
    }
}
