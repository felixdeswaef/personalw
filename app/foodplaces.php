<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class foodplaces extends Model
{
    //
    protected $fillable = [
        'name', 'type', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    public  function hasItems()
    {
        return $this->hasMany(fooditems::class);
    }

}
