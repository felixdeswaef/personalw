<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/tokenr',  array('middleware' => 'cors', 'uses' => 'APILogin@login'))->name('tokenreq');


Route::get('/fplace',  array('middleware' => 'cors', 'uses' => 'FoodplacesController@index'))->name('fplace.all');

Route::post('/fplace',  array('middleware' => 'cors', 'uses' => 'FoodplacesController@store'))->name('fplace.store');

Route::get('/fplace/{place}', array('middleware' => 'cors', 'uses' =>  'FoodplacesController@show'))->name('fplace.show');

Route::put('/fplace/{place}', array('middleware' => 'cors', 'uses' =>  'FoodplacesController@update'))->name('fplace.update');

Route::delete('/fplace/{place}', array('middleware' => 'cors', 'uses' =>  'FoodplacesController@destroy'))->name('fplace.destroy');



Route::get('/fitem', array('middleware' => 'cors', 'uses' => 'FooditemsController@index'))->name('fitem.all');

Route::post('/fitem', array('middleware' => 'cors', 'uses' =>  'FooditemsController@store'))->name('fitem.store');

Route::get('/fitem/{place}', array('middleware' => 'cors', 'uses' =>  'FooditemsController@show'))->name('fitem.show');

Route::put('/fitem/{place}', array('middleware' => 'cors', 'uses' =>  'FooditemsController@update'))->name('fitem.update');

Route::delete('/fitem/{place}', array('middleware' => 'cors', 'uses' =>  'FooditemsController@destroy'))->name('fplace.destroy');



Route::get('/fbill',  array('middleware' => 'cors', 'uses' => 'FoodbillsController@index'))->name('fbill.all');

Route::post('/fbill', array('middleware' => 'cors', 'uses' => 'FoodbillsController@store'))->name('fbill.store');

Route::get('/fbill/{place}',  array('middleware' => 'cors', 'uses' => 'FoodbillsController@show'))->name('fbill.show');

Route::put('/fbill/{place}',  array('middleware' => 'cors', 'uses' => 'FoodbillsController@update'))->name('fbill.update');

Route::delete('/fbill/{place}', array('middleware' => 'cors', 'uses' =>  'FoodbillsController@destroy'))->name('fbill.destroy');




Route::get('/fbitem', array('middleware' => 'cors', 'uses' =>  'FoodbillitemsController@index'))->name('fbitem.all');

Route::post('/fbitem', array('middleware' => 'cors', 'uses' =>  'FoodbillitemsController@store'))->name('fbitem.store');

Route::get('/fbitem/{task}', array('middleware' => 'cors', 'uses' =>  'FoodbillitemsController@show'))->name('fbitem.show');

Route::put('/fbitem/{task}', array('middleware' => 'cors', 'uses' =>  'FoodbillitemsController@update'))->name('fbitem.update');

Route::delete('/fbitem/{task}', array('middleware' => 'cors', 'uses' =>  'FoodbillitemsController@destroy'))->name('fbitem.destroy');

