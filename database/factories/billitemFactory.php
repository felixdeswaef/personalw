<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\foodbillitems::class, function (Faker $faker) {
    return [
        'subselect' =>$faker->dayOfWeek,
        'billid'=> factory(App\foodbills::class)->create()->id,
        'itemid'=>factory(App\fooditems::class)->create()->id,
        'username'=>$faker->name,
        'count'=>$faker->numberBetween(1,10),
        'userid'=> factory(App\User::class)->create()->id,

    ];
});
