<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\foodplaces::class,20)->create();
        factory(\App\User::class,20)->create();
        factory(\App\foodbillitems::class,10)->create();
    }
}
