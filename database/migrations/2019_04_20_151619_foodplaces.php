<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foodplaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("foodplaces", function($table) {
            $table->increments("id");
            $table->string("name", 64);
            $table->string("type",32);
            $table->string("phone",32);
            $table->timestamps();


        });
    }

    public function down() {
        Schema::dropIfExists("foodplaces");
    }
}
