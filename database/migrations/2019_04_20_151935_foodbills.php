<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foodbills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("foodbills", function($table) {
            $table->increments("id");
            $table->string("name", 64);
            $table->integer("placeid")->unsigned();
            $table->foreign('placeid')->references('id')->on('foodplaces')->onDelete('cascade');
            $table->timestamps();

        });
    }

    public function down() {
        Schema::dropIfExists("foodbills");
    }
}
