<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foodbilitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("foodbillitems", function($table) {
            $table->increments("id");
            $table->string("subselect", 64)->default('-');
            $table->string("username", 32)->default('NOUSER');
            $table->integer("count")->default(0);
            $table->integer("billid")->unsigned();
            $table->foreign('billid')->references('id')->on('foodbills')->onDelete('cascade');
            $table->integer("itemid")->unsigned();
            $table->foreign('itemid')->references('id')->on('fooditems')->onDelete('cascade');
            $table->biginteger("userid")->unsigned();
            $table->foreign('userid')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();

        });
    }

    public function down() {
        Schema::dropIfExists("foodbillitems");
    }
}
